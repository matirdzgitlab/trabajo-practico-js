//#region Variables
var numA = 10;
let numB = 5;
const pi = 3.14;
//#endregion

//#region Funciones
const suma = (numA, numB) => numA + numB;
const resta = (numA, numB) => numA - numB;
const multi = (numA, numB) => numA * numB;
const division = (numA, numB) => numA / numB;
//#endregion

//#region Arreglo
let nombres = ['Juan', 'Luis', 'Nico', 'Pedro', 'Pepe'];
//#endregion

//#region Consolas
console.log("Su primer número es " + numA);
console.log("Su segundo número es " + numB);
console.log("Su constante vale " + pi);
console.log("La suma es " + suma(numA, numB));
console.log("La resta es " + resta(numA, numB));
console.log("La multiplicación es " + multi(numA, numB));
console.log("La división es " + division(numA, numB));
for(i = 0; i < nombres.length; i++){
    console.log(nombres[i]);
}
//#endregion